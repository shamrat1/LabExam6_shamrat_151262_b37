<?php

namespace App;

use App\Database as DB;

use PDO;

class BookTitle extends DB
{

    public $id = "";

    public $book_title = "";

    public $author_name = "";


    public function __construct()
    {

        parent::__construct();

    }

    public function fetch()
    {
        $DBH = new PDO("mysql:host=" . $this->host . ";dbname=".$this->dbname, $this->username, $this->password);
        $STH = $DBH->prepare("SELECT * from book_title");
        $STH->execute();

        $STH->setFetchMode(PDO::FETCH_ASSOC);

        while($row = $STH->fetch()){

            echo "ID: ".$row['id'] . "<br>";

            echo "Book Title: ".$row['book_title'] . "<br>";

            echo "Author Name: ".$row['author_name'] . "<br>";
            echo "<br>";
        }
    }


}// end of BookTitle class